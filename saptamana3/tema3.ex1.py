#!tema3ex1
#Implementati transformarea unei liste de tuple intr-un dictionar fara a folosi dict

lista_tuple = [("Ana", 5), ("Maria", 7), ("Alex", 10)]

print("Lista de tuple este: " + str(lista_tuple))

dic = {n[0]: n[1] for n in lista_tuple}

print("Dictionarul este: " + str(dic))
