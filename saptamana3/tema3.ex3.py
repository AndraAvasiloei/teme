#!tema3ex3
'''Scrieti un program care numara aparitiile cuvintelor intr-un text si
le afiseaza la ecran pe 5 cele mai folosite cuvinte.'''


text = str(input('Introdu un text: '))
cuvinte = text.split()
dictionar = {}

for i in cuvinte:
    if i not in dictionar:
        dictionar[i] = 0
    dictionar[i] = dictionar[i] + 1

a = {k: v for k, v in sorted(dictionar.items(), key=lambda item: item[1])}

b = dict()
for key, value in dictionar.items():
    b.setdefault(value, list()).append(key)

c = dict(sorted(b.items()))

print('Lista celor mai folosite 5 cuvite: {}'.format(sorted(c.items())[len(c)-5:len(c)+5]))

