#!tema3ex2
'''
Creati un program care combina doua dictionare in unul singur ?
Aveti grija ce se intampla cand ambele au valori pentru aceeasi cheie.
'''
primul_dictionar = {"Ana" : 2 , "Maria" : 5}
al_doilea_dictionar = {"Ana" : 4, "Caty" : 7}

dictionar_nou = primul_dictionar.copy()
for key, value in al_doilea_dictionar.items():
    if key in dictionar_nou:
        dictionar_nou[key] = (dictionar_nou[key], al_doilea_dictionar[key])
    else:
        dictionar_nou[key] = value

print("{} + {} = {} ".format(primul_dictionar, al_doilea_dictionar, dictionar_nou))
