#!tema3ex5
#Scrieti un program care va elimina dintr-un dictionar cheile care au valori duplicate

dictionar = {
    'key1': 1,
    'key2': 1,
    'key3': 1,
    'key4': 2,
    'key5': 1,
    'key6': 2,
    'key7': 3,
    'key8': 2,
    'key9': 4,
    'key10': 5,
    'key11': 5
}

solutie = {
    'key7': 3,
    'key9': 4
}

rezultat = {}

for key,value in dictionar.items():
    if list(dictionar.values()).count(value)==1:
        rezultat[key] = value

if rezultat == solutie:
    print("Felicitări! {} este răspunsul corect!". format(rezultat))
else:
    print(rezultat)
    print("Eroare: Implementare greșită!")

