#tema2.ex4
#vrem sa ajungem la punctul de origine

def muta(punct, directie, distanta=1):
    # sus: rand -1, coloana +0
    # jos: rand +1, coloana  +0
    # stanga: rand +0, coloana -1
    # dreapta: rand +0, coloana +1
    if directie == 'SUS':
        rand = distanta
        coloana = 0
    elif directie == 'JOS':
        rand = -distanta
        coloana = 0
    elif directie == 'STANGA':
        rand = 0
        coloana = -distanta
    elif directie == 'DREAPTA':
        rand = 0
        coloana = distanta
    else:
        rand = 0
        coloana = 0
    return (punct[0] + rand, punct[1] + coloana)

def main():
    punct = (0, 0)
    istoric = {
        'STANGA': 2,
        'JOS': 2,
        'DREAPTA': 5,
        }
    for directie, distanta in istoric.items():
        punct = muta(punct, directie, distanta)
        print(punct)
    cateta_1 = punct[0]
    cateta_2 = punct[1]
    ipotenuza = (cateta_1 ** 2 + cateta_2 ** 2) **0.5
    print(ipotenuza)
main()
    
