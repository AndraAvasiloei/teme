#tema2.ex1.cu text primit si verificat
'''Scrieți un program Python care să primească un text și
să afișeze pe ecran în ordine alfabetică toate cuvintele cu
un număr impar de caractere și frecvența unei vocale egală cu 3.

Condiții:
cuvântul trebuie sa aibă număr impar de caractere;
cuvântul trebuie să conțină cel puțin o vocală cu numărul de apariții egal cu 3;

Cerințe:
trebuie să găsim toate cuvintele ce respectă condițiile de mai sus
trebuie să le afișăm pe ecran în ordine alfabetică'''

text = input('Introdu un text: ') #introducerea unui text
vocale = ['a', 'ă', 'î', 'e', 'i', 'o', 'u'] #am o lista de vocale
lista = text.split()         #am creat o lista de cuvinte
sortata = sorted(lista)           #lista de cuvinte sortata in ordine alfabetica
lista_finala = []

def numara_vocale(cuvant, numar):
    if (len(cuvant) % 2) and len(cuvant) > 2:   #numar impar de litere
        for v in vocale:
            if cuvant.count(v) > 2 and cuvant.count(v) < 4:
                lista_finala.append(cuvant)
                break
for i in range(int(len(sortata))):
    numara_vocale(sortata[i], len(sortata[i]))

if lista_finala != []:
    print('Lista cuvintelor ce respecta regulile, ordonata alfabetic: {}'.format(lista_finala))
else:
    print('Nu exista cuvinte care sa respecte regulile, in textul {},'.format(text))
