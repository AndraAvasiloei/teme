#tema2.ex2
#2 siruri de caractere pe care le comparam

valoare1 = input("Introdu un sir de caractere: ")
valoare2 = input('Introdu inca un sir de caractere: ')

list1 = list(valoare1)
list2 = list(valoare2)
ok2 = False 

for i in list1:
    ok = False
    for j in list2:
        if i == j:
            ok = True
    if ok == False:
        print('Nu se gasesc toate elementele din valoare1 in valoare2.')
        ok2 = True
        break
if ok2 == False:
    print('Se gasesc toate elementele din valoare1 in valoare2.')

for j in list2:
    ok = False
    for i in list1:
        if j == i:
            ok = True
    if ok == False:
        print('Nu se gasesc toate elementele din valoare2 in valoare1.')
        ok2 = True
        break
if ok2 == False:
    print('Se gasesc toate elementele din valoare2 in valoare1.')
