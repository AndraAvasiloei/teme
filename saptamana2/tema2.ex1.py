#tema2.ex1
#Scrieți un program Python care să primească un text și să afișeze pe ecran în ordine alfabetică toate cuvintele cu un număr impar de caractere și frecvența unei vocale egală cu 3.

text = '''
scrieti un program python care sa primească un text și să
afișeze pe ecran în ordine alfabetică toate cuvintele cu
un număr impar de caractere și frecvența unei vocale egală
cu 3.'''

vocale = ['a', 'ă', 'î', 'e', 'i', 'o', 'u'] #am o lista de vocale
lista = text.split()         #am creat o lista de cuvinte
sortata = sorted(lista)           #lista de cuvinte sortata in ordine alfabetica
c = 0                  

for i in sortata:
    if len(i) % 2 == 1:   #numar impar de litere
        for j in i:
            if j in vocale:
                c = c+1
        if c == 3:
            print(i)
        c = 0

