#!tema1.ex3.cu verificarea informatiilor primite
#afla-l pe x din operatia a * x + b = c

def este_numar(text):
  if not text:
    return False
  if text.startswith('0'):
    return False
  for caracter in text:
    if not ord('0') <= ord(caracter) <= ord('9'):
      return False
  return True

def citeste_numar(cifra):
  while True:  
    valoare = input('Introduceti numar {}: '.format(cifra))
    if not este_numar(valoare):
      print('Valoare invalida.')
      print('Incearca din nou')
      continue
    break
  return int(valoare)

def main():
    a = float(citeste_numar("a"))
    b = float(citeste_numar("b"))
    c = float(citeste_numar("c"))
    print("Rezolvam ecuatia: {} * x + {} = {}".format(a, b, c))
    x = (c - b) / a
    print ("({} - {}) / {}".format(c, b, a, x))
    print ("x =", x)

main()
