#tema1.ex1.cu verificarea informatiilor primite
#Scrieți un program Python care să interschimbe două valori

def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True

def citeste_numar():
    while True:
        valoare = input("Alege un numar: ")
        if not este_numar(valoare):
            print ("Valoare invalida.")
            print("Incearca din nou.")
            continue
        break
    return int(valoare)
valoare1 = citeste_numar()
valoare2 = citeste_numar()
valoare1, valoare2 = valoare2, valoare1

print("Prima valoare este: ", valoare1)
print("A doua valoare este: ", valoare2)
