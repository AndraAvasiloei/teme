#!tema1.ex5.cu verificarea informatiilor primite
#Scrieți un program Python care să verifice dacă un număr este prim.

def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True
    
def citeste_numar():
    while True:
        valoare = input('Introdu numarul ales: ')
        if not este_numar(valoare):
            print('Valoare invalida.')
            print('Incearca din nou.')
            continue
        break
    return int(valoare)

def prim(valoare):
    divizori = 0
    d = 1
    while d <= n:
        if valoare % d == 0:
            divizori += 1    
            if divizori == 3:
                break
        d += 1
    if divizori == 2:
        return True
    else:
        return False

if prim(valoare):
    print("Numarul {} este prim!".format(valoare))
else:
    print("Numarul {} nu este prim!".format(valoare))
    
