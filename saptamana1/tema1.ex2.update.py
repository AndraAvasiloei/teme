#!tema1.ex2. cu verificarea informatiilor primite
#valoarea min si  max din 3 numere primite de la utilizator

def este_numar(text):
  if not text:
    return False
  if text.startswith('0'):
    return False
  for caracter in text:
    if not ord('0') <= ord(caracter) <= ord('9'):
      return False
  return True

def citeste_numar():
  while True:  
    valoare = input('Introduceti un numar: ')
    if not este_numar(valoare):
      print('Valoare invalida.')
      print('Incearca din nou')
      continue
    break
  return int(valoare)

def minim(numere):
  rezultat = numere[0]
  for numar in numere:
    if numar < rezultat:
      rezultat = numar
  return rezultat

def maxim(numere):
  rezultat = numere[0]
  for numar in numere:
    if numar > rezultat:
      rezultat = numar
  return rezultat

def start():
  numere = []
  for _ in range(3):
    numar = citeste_numar()
    numere.append(numar)
  print('Numerele citite sunt: %s' % numere)
  print('Valoarea maxima este: %d' % maxim(numere))
  print('Valoarea minima este: %d' % minim(numere))
start()
