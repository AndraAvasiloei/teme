#!tema1.ex5
#Scrieți un program Python care să verifice dacă un număr este prim.

n = int(input("Alege un numar:"))

def prim(n):
    divizori = 0
    d = 1
    while d <= n:
        if n % d == 0:
            divizori += 1    
            if divizori == 3:
                break
        d += 1
    if divizori == 2:
        return True
    else:
        return False

if prim(n):
    print("Numarul {} este prim!".format(n))
else:
    print("Numarul {} nu este prim!".format(n))
    
