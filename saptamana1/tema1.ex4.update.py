#!tema1.ex4.cu verificarea informatiilor primite
#Scrieți un program Python care să determine dacă un număr este par.

def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True
    
def citeste_numar():
    while True:
        valoare = input('Introdu numarul ales: ')
        if not este_numar(valoare):
            print('Valoare invalida.')
            print('Incearca din nou.')
            continue
        break
    return int(valoare)
    
def main():
    numar = int(citeste_numar())
    if (numar % 2) == 0:
        print ("Numarul {} este par".format(numar))
    else:
        print ("Numarul {} este impar".format(numar))

main()
